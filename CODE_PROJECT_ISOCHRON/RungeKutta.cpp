//### IMPLEMENTATION OF RK METHODS                 ###//
//###                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam   ###//
//###                                              ###//

#include "../HEADERFILES_PROJECT_ISOCHRON/Main_isochron.h"
 
void rk1(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin){
 vector<double> dydx1(N);
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t, pertVar,epsin);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + dt*dydx1[i];
    }
   t = t + dt;
  }
}

void rk2(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin){
 vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> y1(N);
 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t, pertVar,epsin);
   for(int i=0; i<N; i++)
    {
     y1[i] = y[i] + 0.5*dt*dydx1[i];
    }
   t = t + 0.5*dt;
   (*derives)(y1,dydx2,N,t, pertVar,epsin);
    for(int i=0; i<N; i++)
    {
     y[i] = y[i] + dt*dydx2[i];
    }
   t = t + 0.5*dt;
  }
}

void rk3(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin){
 vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> dydx3(N);
 vector<double> y1(N);
 vector<double> y2(N);

 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t, pertVar,epsin);//k1
   for(int i=0; i<N; i++)
    {
     y1[i] = y[i] + 0.5*dt*dydx1[i];//for k2
    }
   t = t + 0.5*dt;
   (*derives)(y1,dydx2,N,t, pertVar,epsin);//k2
    for(int i=0; i<N; i++)
    {
     y2[i] = y[i] + 2.0*dt*dydx2[i] - dt*dydx1[i];//for k3
    }
   t = t + 0.5*dt;
   (*derives)(y2,dydx3,N,t, pertVar,epsin);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((dt/6.0)*dydx1[i] + (2.0*dt/3.0)*dydx2[i] + (dt/6.0)*dydx3[i]);
    }
  }
}

void rk4(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin){
 vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> dydx3(N);
 vector<double> dydx4(N);
 vector<double> y1(N);
 vector<double> y2(N);
 vector<double> y3(N);

 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t, pertVar,epsin);//k1
   for(int i=0; i<N; i++)
    {
     y1[i] = y[i] + 0.5*dt*dydx1[i];//for k2
    }
   t = t + 0.5*dt;
   (*derives)(y1,dydx2,N,t, pertVar,epsin);//k2
    for(int i=0; i<N; i++)
    {
     y2[i] = y[i] + 0.5*dt*dydx2[i] ;//for k3
    }
   (*derives)(y2,dydx3,N,t, pertVar,epsin);
   for(int i=0; i<N; i++)
    {
     y3[i] = y[i] + dt*dydx3[i];
    }
   t = t + 0.5*dt;
   (*derives)(y3,dydx4,N,t, pertVar,epsin);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((dt/6.0)*dydx1[i] + (dt/3.0)*dydx2[i] + (dt/3.0)*dydx3[i] + (dt/6.0)*dydx4[i]);
    }
  }
}

void rk4var(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin){
 vector<double> dydx1(N);
 vector<double> dydx2(N);
 vector<double> dydx3(N);
 vector<double> dydx4(N);
 vector<double> y1(N);
 vector<double> y2(N);
 vector<double> y3(N);

 for(int j=0; j<Nsim; j++)
  {
   (*derives)(y, dydx1,N,t, pertVar,epsin);//k1
   for(int i=0; i<N; i++)
    {
     y1[i] = y[i] + 0.5*dt*dydx1[i];//for k2
    }
   t = t + 0.5*dt;
   (*derives)(y1,dydx2,N,t, pertVar,epsin);//k2
    for(int i=0; i<N; i++)
    {
     y2[i] = y[i] + 0.5*dt*dydx2[i] ;//for k3
    }
   (*derives)(y2,dydx3,N,t, pertVar,epsin);
   for(int i=0; i<N; i++)
    {
     y3[i] = y[i] + dt*dydx3[i];
    }
   t = t + 0.5*dt;
   (*derives)(y3,dydx4,N,t, pertVar,epsin);
   for(int i=0; i<N; i++)
    {
     y[i] = y[i] + ((dt/6.0)*dydx1[i] + (dt/3.0)*dydx2[i] + (dt/3.0)*dydx3[i] + (dt/6.0)*dydx4[i]);
    }
  }
}

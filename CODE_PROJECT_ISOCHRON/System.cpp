//### IMPLEMENTATION OF MODEL SYSTEM FOR ISOCHRONES ###//
//###                                               ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam    ###//
//###                                               ###//

#include "../HEADERFILES_PROJECT_ISOCHRON/Main_isochron.h"

void Initializer(vector<double> &y, vector<double> &dydx, double &t, const double yinit, const int N){
 for(int i=0;i<N;i=i+1)
  {
   //y[2*i] = yinit*cos(double(2*i)/(double(N))*2.0*pi);
   //y[2*i+1] = yinit*sin(double(2*i)/(double(N))*2.0*pi);
   y[i]    = yinit;
   dydx[i] = yinit;//*sin(0.1+double(i)/(double(N))*2.0*pi);
  }
 /*for(int i=0;i<N;i++)
  {
   if(i<2)
   {
    y[i] = yinit;
   }
   else
   {
    y[i] = pert;
   }    
   dydx[i] = 0.0;
  }*/
 t=0.0;
 for(int i=0; i<N;i++)
  {
   cout << "Initialized: y[" << i << "]=" << y[i] << " dydx[" << i << "]=" << dydx[i] << "\n";
  }
}

//biological fun ctions for the Morris-Lecar neuron model//

double M_inf(double x)
 {
  return 0.5*(1.0 + tanh((x-V1)/V2));
 }

double W_inf(double x)
 {
  return 0.5*(1.0 + tanh((x-V3)/V4));  
 }

double Lambda(double x)
 {
  return (cosh(0.5*((x-V3)/V4)))*Phi;
 }


void sys(vector<double> yy, vector<double> &dyydx, const int N, double &t, double &pertVar, double &epsin){
//Stuart-Landau//   eps   Omegaint          
//dyydx[0] = epsin*(yy[0]-(yy[0]*yy[0]+yy[1]*yy[1])*yy[0])-Omegaint*yy[1];//Omegaint*yy[1];
//dyydx[1] = epsin*(yy[1]-(yy[0]*yy[0]+yy[1]*yy[1])*yy[1])+Omegaint*yy[0];//Omegaint*yy[0]+pert*sin(Omega*t);

//Van der Pol//
//dyydx[0] = yy[1];
//dyydx[1] = epsin*(1.0-yy[0]*yy[0])*yy[1]-yy[0];    

//Rayleigh//
//dyydx[0] = yy[1];
//dyydx[1] = epsin*(1.0-(yy[1]*yy[1]))*yy[1] - yy[0]; 

//Morris-Lecar neuron modell//
 dyydx[0] = I - gL*(yy[0]-VL) -gK*yy[1]*(yy[0] - VK) -gCa*M_inf(yy[0])*(yy[0]-VCa);
 dyydx[1] = Lambda(yy[0])*(W_inf(yy[0])-yy[1]); 

//FitzHugh-Nagumo
//dyydx[0] = yy[0] - yy[0]*yy[0]*yy[0]/3.0 - yy[1];// + Iext;
//dyydx[1] = (yy[0] + epsin)/12.5;  //0.0*yy[1])
}

void Henontrick(vector<double> yy, vector<double> &dyydx, const int N, double &t, double &pertVar, double &epsin){
//Stuart-Landau//   eps   Omegaint          
//dyydx[1] = 1.0/(epsin*(t-(yy[0]*yy[0]+t*t)*t)+Omegaint*yy[0]);
//dyydx[0] = t*dyydx[1];
//Van der Pol//
//dyydx[1] = 1.0/(epsin*(1.0-yy[0]*yy[0])*t-yy[0]);
//dyydx[0] = t*dyydx[1];    
//Rayleigh//    
//dyydx[1] = 1.0/(epsin*(1.0-(t*t))*t - yy[0]);
//dyydx[0] = t*dyydx[1];
//Morris-Lecar neuron modell//
 dyydx[1] = 1.0/(Lambda(yy[0])*(W_inf(yy[0])-t));
 dyydx[0] = (I - gL*(yy[0]-VL) -gK*t*(yy[0] - VK) -gCa*M_inf(yy[0])*(yy[0]-VCa))*dyydx[1];
//FitzHugh-Nagumo
//dyydx[1] = 1.0/((yy[0]+epsin)/12.5);
//dyydx[0] = (yy[0]-yy[0]*yy[0]*yy[0]/3.0 - t)*dyydx[1];// + Iext
}

void FindZero(vector<double> &y,double &t, double &pertVar, double &epsin)
{
 double xp(0.0), yp(0.0), tp(0.0);
   do {
       xp=y[0]; yp=y[1]; tp=t;
       rk4(sys,y,Nsim,t,N,dt,pertVar,epsin);//integrate unforced system
       //xp=y[0]; yp=y[1]; tp=t;
       //cout << "yp= " << yp << " y[1]= " << y[1] << " " << tp << "\n";
      }      
   while(!(yp<0.25 && y[1]>0.25));//FOR Mor-Lec
   //while(!(yp<0.0 && y[1]>0.0));//For VdP, Ray, SL
  
   //cout << "y[0]= " << y[0] << " y[1]= " << y[1] << "\n";
  
   y[1]=tp;  y[0]=xp;
   //double backstep(-yp); //SL,Ray,VdP
   double backstep(0.25-yp);
   rk4(Henontrick,y,Nsim,yp,N,backstep,pertVar,epsin);
   t=y[1]; 
   //y[1]=0.0;//by definition of the Henon Trick, y[1] is zero after backward integration (SL,VdP,Ray)
   y[1]=0.25;//Mor-Lec.
}


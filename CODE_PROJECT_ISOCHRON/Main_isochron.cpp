//### IMPLEMENTATION OF ISOCHRON CALCULATION       ###//
//###                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam   ###//
//###                                              ###//
 
 #include "../HEADERFILES_PROJECT_ISOCHRON/Main_isochron.h"
 #include <random>

double my_mod(double x, double y)
 {
  return x - floor(x/y)*y;
 }

 int main(int argc, char* argv[])
  {
   
   //NOTE: also give argv[1] as experiment  number;
   double tolerance = strtod(argv[2],NULL); //allowed phase variation (not bigger than dt)
   double epsin = strtod(argv[3],NULL);     //stability parameter (VdP, SL)
   // set rectangle paramters for measurement of isochrone
   double minX = strtod(argv[4],NULL); 
   double maxX = strtod(argv[5],NULL); 
   double minY = strtod(argv[6],NULL); 
   double maxY = strtod(argv[7],NULL); 
   // set number of points for one direction stepX*stepY is the maximum number of isochrones
   int stepX = int(strtod(argv[8],NULL));    // how many different points in x
   int stepY = int(strtod(argv[9],NULL));    // how many different points in y
   int mo = int(strtod(argv[10],NULL));      // at each mo/nst_a multiple of 2pi the isochrone is searched
   int nst_a = int(strtod(argv[11],NULL));   // integration step
   int Np = int(strtod(argv[12],NULL));      //
   double Mp = strtod(argv[13],NULL);        // Mp < number of points in an isochrone < 2Mp to be found
   //variables
   vector<double> y(N);
   vector<double> yy(N);
   vector<double> dydx(N);
   //to be filled before isochrone estimation
   vector<double> LimCX;
   vector<double> LimCY;
   vector<double> Phase;
   
   //for PRC determination
   
   srand(time(NULL));
   double t;
    
   std::stringstream out0; out0 << "Isochrones" << argv[1] << "_"<< maxX << "_" << maxY << "_" << minX << "_" << minY << ".dat";
   std::string name0 = out0.str(); ofstream fout(name0,ios::out);
   fout.setf(ios::scientific); fout.precision(15);
  
   //### determine the period time ###//
   Initializer(y, dydx, t, yinit, N);
   double pertVar(0.0);
   for(int k=0;k<300000;k++)        //  transient for unperturbed system
    {
     rk4(sys,y,Nsim,t,N,dt,pertVar,epsin);//unperturbed system
    }   
    
   double t1(0.0), T(0.0), step_a(0.0), phi(0.0);
   for(int k=0; k<10; k++)
    {
     FindZero(y,t,pertVar,epsin);   //find first zero crossing
     t1=t;
     FindZero(y,t,pertVar,epsin);   //find second zero crossing
     T=t-t1;                        //calculate Period time on cycle
    }
   step_a=T/(double(nst_a));        //calculate integration step for later phase 
  
   //### construct Limit cycle with phase ###//

   //cout << "calculate limit cycle \n";
   t=0.0;
   for(int k=0; k<nst_a; k++)
    {
     rk4(sys,y,Nsim,t,N,step_a,pertVar,epsin);
     if(k%mo==0)
      {
       LimCX.push_back(y[0]);
       LimCY.push_back(y[1]);
       Phase.push_back(2.0*pi*(t)/T);
      }
    }

   double dX = (maxX-minX)/(double(stepX));
   double dY = (maxY-minY)/(double(stepY));
   double deltaT(0.0), xini(0.0), yini(0.0);
   int go_on(0);
   vector<int> Pcount(Phase.size());//counts how many points are found within a tollerance for each isochrone
   //### Using rastering ###//
   do
    {
     yy[0] = minX + double(rand()%stepX)*dX;
     yy[1] = minY + double(rand()%stepY)*dY;
     xini=yy[0]; yini=yy[1];
     t=0.0;
     for(int m=0; m<Np*nst_a; m++) // integrate initial condition for long time
      {
       rk4(sys,yy,Nsim,t,N,step_a,pertVar,epsin);
      }
     deltaT = t;
     FindZero(yy,t,pertVar,epsin); //find next zero crossing
     deltaT = t-deltaT;
     go_on=0;
     for(int n=0;n<Phase.size();n++)
      {
       if(abs(2.0*pi*deltaT/T-Phase[n])<=tolerance)
        {
     	 if(Pcount[n]<2*int(Mp)) fout << xini << " " << yini << " " << Phase[n] << "\n";
         Pcount[n] +=1;
	    }
       if(Pcount[n]<int(Mp)) go_on=1;
      }
    }
   while(go_on==1);
   return 0;
  }

//### IMPLEMENTATION OF BOX MULLER NOISE           ###//
//###                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam   ###//
//###                                              ###//

#include "../HEADERFILES_PROJECT_ISOCHRON/Main_isochron.h"

vector<double> noiseGauss(double mean, double sigma)
{
vector<double> rands(2);
double u1,u2;
const double pi(atan(1.0)*4.0);
u1 = rand() * (1.0/RAND_MAX);
u2 = rand() * (1.0/RAND_MAX);
rands[0] = mean + sigma * sqrt(-2.0*log(u1)) * cos(2.0*pi*u2);
rands[1] = mean + sigma * sqrt(-2.0*log(u1)) * sin(2.0*pi*u2);
return rands;
}



 
 #this script compiles the code for estimation of isochrones
 
 HEADERPATH='../HEADERFILES_PROJECT_ISOCHRON/'
 CODEPATH='../CODE_PROJECT_ISOCHRON/'
 
 g++ $HEADERPATH/Main_isochron.h $CODEPATH/Noise.cpp $CODEPATH/RungeKutta.cpp $CODEPATH/System.cpp $CODEPATH/Main_isochron.cpp -o ISOCHRONE_ML -std=c++11 -O

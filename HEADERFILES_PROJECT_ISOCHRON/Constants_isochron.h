// ### CONSTANT DEFINITIONS ISOCHRON                  ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

const int N(2);
const double Nsim(1);
const double Ntime(300000);
const double Tbegin(0.0);
const double Tend(0.0);
const double dt(0.002);
const double yinit(1.2);
const double eps(10.0); //given as argv
const double Omega(0.3*sqrt(5.0));//sqrt(3.0));//frequency of forcing
const double Omegaint(1.6);//frequency of internal oscillation SL oscilator
const double pert(0.1);//given as argv

//### PARAMETERS FOR NEURONAL MODELS ###//

// MORRIS LECAR //
//Paramters from "Bifurcations in Morris–Lecar neuron model"
//               "FORCED SYNCHRONIZATION IN MORRIS–LECAR NEURONS"
//TYPEI
const double t_re(20.0);
const double C(20.0);
const double I(50.0/C*t_re);//#
const double gL(2.0/C*t_re);//#
const double gK(8.0/C*t_re);//#
const double gCa(4.0/C*t_re);//#
const double V1(-1.2);//#
const double V2(18.0);//#
const double V3(12.0);//###### -> class 1
const double V4(17.4);//#
const double VL(-60.0);//#
const double VK(-80.0);//#
const double VCa(120.0);//#
const double Phi (1.0/15.0*t_re);//#
//*/

//Paramters from Rok Cestniks thesis. TODO: find his source...
//TYPEI
/*const double t_re(1.0);
const double C(1.0);
const double I(0.07/C*t_re);//#
const double gL(0.5/C*t_re);//#
const double gK(2.0/C*t_re);//#
const double gCa(1.33/C*t_re);//#
const double V1(-0.01);//#
const double V2(0.15);//#
const double V3(0.1);//###### -> class 1
const double V4(0.145);//#
const double VL(-0.5);//#
const double VK(-0.7);//#
const double VCa(1.0);//#
const double Phi (1.0/3.0*t_re);//#
//*/




// ### MAIN HEADER FOR DATA GENERATION                ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

//### BASIC SELF-DEFINED HEADERS ###//
#include "Constants_isochron.h"
#include "RungeKutta.h"
#include "DeclarationFunctions_isochron.h"

const double pi(atan(1.0)*4.0); //pi
